import Layout from 'antd/lib/layout/layout';
import Decrypt from '../Views/Decrypt/Decrypt';
import Encrypt from '../Views/Encrypt/Encrypt';
import Home from '../Views/Home/Home';
import Footer from '../Views/Layout/Footer/Footer';
import Header from '../Views/Layout/Header/Header';
import MainLayout from '../Views/Layout/MainLayout';
import './App.css';

function App() {
  return (
    <Layout className='container'>
      <MainLayout>
        <Header />
        <div id='home'>
        <Home/>
        </div>
        <div id='encrypt'>
        <Encrypt/>
        </div>
        <div id='decrypt'><Decrypt/></div>
      </MainLayout>
      <Footer />
    </Layout>

  );
}

export default App;
