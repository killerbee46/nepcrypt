import { Route, RouteChildrenProps, Switch } from "react-router";
import { BrowserRouter as Router } from "react-router-dom";
import Decrypt from "../Views/Decrypt/Decrypt";
import Encrypt from "../Views/Encrypt/Encrypt";
import Home from "../Views/Home/Home";
import MainLayout from "../Views/Layout/MainLayout";

export const MainRoute = () => {
  return(
    <Router>
        <Switch> 
          <Route path="/">
            <Home />
           </Route>
           <Route path="/encrypt">
            <Encrypt />
          </Route>
          <Route path="/decrypt">
              <Decrypt />
            </Route> 
        </Switch>
    </Router>
  )
}
export default MainRoute;