import { Typography } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import './Home.css';

export default function Home(){
    return(
        <Content className="homecontent">
            <Typography.Title level={1} className="greeting">Welcome to <br></br>NEPCrypt</Typography.Title>
            <Typography.Title level={4} className="function">Scroll Down for encryption or decryption</Typography.Title>
            </Content>
    );
}