import { Col, Row, Space } from 'antd';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import './Header.css';

export default function Header() {
    return (
        <Row className="nav" align='middle'>
            <Router>
                <Col className='nav__logo'>
                    <a href='#home'>NEPCrypt</a>
                </Col>
                <Col className='nav__menu'>
                    <Space size='large'>
                        <a href='#home'>Home</a>
                        <a href='#encrypt'>Encrypt</a>
                        <a href='#decrypt'>Decrypt</a>
                    </Space>
                </Col>
            </Router>
        </Row>
    );
}