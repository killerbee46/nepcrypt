import { Col, Row } from "antd";
import Layout, { Content } from "antd/lib/layout/layout";

export const MainLayout = (props:any) =>{
    return(
        <Layout>
            <Content>
                <Row style={{backgroundImage:"url('https://images.unsplash.com/photo-1526374965328-7f61d4dc18c5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZGF0YSUyMGVuY3J5cHRpb258ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80')", backgroundRepeat: 'cover',
  backgroundAttachment: 'fixed', height: "fit-content"}} justify='center'>
                    <Col span={20}>
                    {props.children}
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
}
export default MainLayout;