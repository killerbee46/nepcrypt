import { Typography } from "antd";
import './Footer.css';
import { Content } from "antd/lib/layout/layout";

export default function Footer(){
    return(
        <Content className='footer'>
            <Typography.Text className='f_text'>&copy; 2021 All Rights Reserved by Sugam Bhandari</Typography.Text>
        </Content>
    );
}