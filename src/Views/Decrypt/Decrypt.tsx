import { Button, Col, Input, Row, Space, Typography } from 'antd';
import Layout, { Content } from 'antd/lib/layout/layout';
import { useState } from 'react';
import './Decrypt.css';

export default function Decrypt() {
    const [text, setText] = useState('');
    const [dec, setDec] = useState('');
    function decrypt(){
        var dec = text;
        setDec(dec);
        }
    return (
        <Content className="decryption">
            <Typography.Title level={1} className='title'>Decryption</Typography.Title>
            <Typography.Text className='dec_sug'>Please enter your message to decrypt </Typography.Text><br></br>
            <Input.TextArea className='to_dec'
                onChange={e => setText(e.target.value)}
            ></Input.TextArea>
            <Typography.Text className='dec_result'>Click <Typography.Text><Button className='dec_button'
                onClick={(e: any) => {

                    if (text) {
                        decrypt();
                    }
                    else {
                        alert("The field cannot be empty..")
                    }
                }}>Decrypt</Button></Typography.Text> to see your result here</Typography.Text><br></br>
            <Input.TextArea className='decrypted' value={dec} />
        </Content>
    );
}