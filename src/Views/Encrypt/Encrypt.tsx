import { Button, Input, Typography } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import { useState } from 'react';
import './Encrypt.css';

export default function Encrypt(){
    const [text, setText] = useState('');
    const [enc, setEnc] = useState('');
    function encrypt(){
        var enc = text;
        setEnc(enc);
        }
    return(
        <Content className="encryption">
            <Typography.Title level={1} className='title'>Encryption</Typography.Title>
                <Typography.Text className='enc_sug'>Please enter your message to ENCRYPT </Typography.Text><br></br>
                <Input.TextArea className='to_enc'
                onChange={e=>setText(e.target.value)}
                value={text}></Input.TextArea>
                <Typography.Text className='enc_result'>Click <Typography.Text><Button className='enc_button'
                onClick={(e: any) => {

                    if (text) {
                        encrypt();
                    }
                    else {
                        alert("The field cannot be empty..")
                    }
                }}>Encrypt</Button></Typography.Text> to see your result here</Typography.Text><br></br>
                <Input.TextArea className='encrypted' value={enc} />
            </Content>
    );
}
